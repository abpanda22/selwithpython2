from selenium import webdriver
driver = webdriver.Chrome(executable_path=r"chromedriver.exe")
driver.get("https://nxtgenaiacademy.com/multiplewindows/")
window=driver.current_window_handle
driver.find_element_by_name("newbrowserwindow").click()
windows=driver.window_handles
print(len(windows))
for win in windows:
    if win!=window:
        driver.switch_to.window(win)
        print(driver.title)
        driver.close()
driver.switch_to.window(window)
print(driver.title)
driver.close()

