from selenium import webdriver
import pytest
import pytest_html
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
@pytest.mark.smoke
def test_sel():
    driver = webdriver.Chrome(executable_path=r'chromedriver.exe')
    driver.get("https://www.thetestingworld.com/")
    driver.maximize_window()
    driver.find_element_by_xpath("//div[@id='ja-wrapper']/div[2]//div[2]/ul/li[2]").click()
    driver.find_element_by_xpath("//div[@id='ja-wrapper']/div[2]//div[2]/ul/li[2]/div//a").click()
    element = WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.XPATH,"//div[@id='ja-wrapper']/div[2]//div[2]/ul/li[3]")))
    act = ActionChains(driver)
    ele ="//div[@id='ja-wrapper']/div[2]//div[2]/ul/li[3]"
    act.move_to_element(driver.find_element_by_xpath(ele)).move_to_element(driver.find_element_by_xpath(ele+"/div//a")).move_to_element(driver.find_element_by_xpath(ele+"/div//li/div//a")).click(driver.find_element_by_xpath(ele+"/div//li/div//a")).perform()
    # driver.find_element_by_xpath("//div[@class='related-links']/a[3]").click()